package com.srin.simplemultiscreen;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.samsung.multiscreen.application.Application;
import com.samsung.multiscreen.application.Application.Status;
import com.samsung.multiscreen.application.ApplicationAsyncResult;
import com.samsung.multiscreen.application.ApplicationError;
import com.samsung.multiscreen.channel.Channel;
import com.samsung.multiscreen.channel.ChannelAsyncResult;
import com.samsung.multiscreen.channel.ChannelClient;
import com.samsung.multiscreen.channel.ChannelClients;
import com.samsung.multiscreen.channel.ChannelError;
import com.samsung.multiscreen.device.Device;
import com.samsung.multiscreen.device.DeviceAsyncResult;
import com.samsung.multiscreen.device.DeviceError;

public class MainActivity extends Activity {
    private static final String CHANNEL_ID = "com.samsung.multiscreen.chatbinus";
    // RUN TITLE
    private static final String RUN_TITLE = "ChatBinus";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sample();
    }

    private void sample() {
        Log.d("search", "Search");
        String pinCode = "567147";
        Device.getByCode(pinCode, new DeviceAsyncResult<Device>() {
            @Override
            public void onResult(Device device) {
                Log.d("search", "=======================================");
                Log.d("search", String.format("Device Name : %s", device.getName()));
                Log.d("search", String.format("Service URI : %s", device.getServiceURI()));

                // connect(device);
            }

            @Override
            public void onError(DeviceError error) {
                Log.e("search", "Search Failed : " + error.getMessage());

            }
        });

        // 192.168.43.181
        Device.getDevice(URI.create("http://<YOUR_TV_EMULATOR_IP_ADDRESS>:8001/ms/1.0/"), new DeviceAsyncResult<Device>() {

            @Override
            public void onResult(Device device) {
                Log.d("get_device", "=======================================");
                Log.d("get_device", String.format("Device Name : %s", device.getName()));
                Log.d("get_device", String.format("Service URI : %s", device.getServiceURI()));
                checkAPPStatus(device);
            }

            @Override
            public void onError(DeviceError error) {
                Log.e("get_device", "getDevice Failed : " + error.getMessage());
            }
        });
    }

    private void launchApp(final Application app, final Device device) {
        app.launch(new ApplicationAsyncResult<Boolean>() {

            @Override
            public void onResult(Boolean status) {
                Log.d("launchApp", String.format("Launch Status : %s", status));
                connect(device, app);
            }

            @Override
            public void onError(ApplicationError error) {
                Log.e("launchApp", "Launch Failed : " + error.getMessage());
            }
        });
    }

    private void checkAPPStatus(final Device device) {
        device.getApplication(RUN_TITLE, new DeviceAsyncResult<Application>() {

            @Override
            public void onResult(final Application app) {
                Log.d("checkAPPStatus", String.format("Last Known Status : %s", app.getLastKnownStatus()));

                // CHECK UPDATED STATUS
                app.updateStatus(new ApplicationAsyncResult<Application.Status>() {
                    @Override
                    public void onResult(Status status) {
                        Log.d("updateStatus", String.format("Current Status : %s", status));
                        switch (status){
                            case STOPPED:
                                launchApp(app, device);
                                break;
                            case RUNNING:
                                connect(device, app);
                                break;
                            default:
                                break;
                        }
                    }

                    @Override
                    public void onError(ApplicationError error) {
                        Log.e("updateStatus", "Failed : " + error.getMessage());
                    }
                });
            }

            @Override
            public void onError(DeviceError error) {
                Log.e("checkAPPStatus", "Failed : " + error.getMessage());
            }
        });
    }

    private void sendMessage(Channel channel) {
        boolean encryptMessage = true;
        JSONObject jsonMessage = new JSONObject();
        try {
            jsonMessage.put("type", "sendimage");
            jsonMessage.put("text", "data:image/bmp;base64," + "<data gambar>");
            String messageText = jsonMessage.toString();
            channel.sendToAll(messageText, encryptMessage);
            Log.d("sendMessage", "Message Sent");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void connect(Device device, final Application app) {
        Log.d("connect", "Connecting");
        Map<String, String> clientAttributes2 = new HashMap<String, String>();
        clientAttributes2.put("name", "Mr.X");
        device.connectToChannel(CHANNEL_ID, clientAttributes2, new DeviceAsyncResult<Channel>() {
            @Override
            public void onResult(final Channel channel) {
                Log.d("connect", "Connected");
                ChannelClients clients = channel.getClients();
                for (ChannelClient client : clients.list()) {
                    Log.d("connect", "===============================================");
                    Log.d("connect", String.format("Client ID : %s", client.getId()));
                    Log.d("connect", String.format("Connect Time : %s", client.getConnectTime()));
                }
                sendMessage(channel);
                disconnect(channel, app);

            }

            @Override
            public void onError(final DeviceError error) {
                Log.e("connect", String.format("Connect Failed : %s", error.getMessage()));
            }
        });
    }

    private void disconnect(Channel channel, final Application app) {
        channel.disconnect(new ChannelAsyncResult<Boolean>() {

            @Override
            public void onResult(Boolean success) {
                Log.e("disconnect", String.format("disconnect : %s", success));
                terminateApp(app);
            }

            @Override
            public void onError(ChannelError error) {
                Log.e("disconnect", String.format("disconnect Failed : %s", error.getMessage()));

            }
        });
    }

    private void terminateApp(Application app) {
        app.terminate(new ApplicationAsyncResult<Boolean>() {

            @Override
            public void onResult(Boolean success) {
                Log.e("terminateApp", String.format("status : %s", success));
            }

            @Override
            public void onError(ApplicationError error) {
                Log.e("terminateApp", String.format("terminateApp Failed : %s", error.getMessage()));
            }
        });
    }
}
